#!/bin/bash

REFERRER="nisewatt.top"
RED="\033[1;31m"
GREEN="\033[1;32m"
GREY="\e[0;35m"
NC="\033[0m"

function usage {
        echo -e "Usage:\n"
        echo -e "$0 [-p]|[-u] [username]\n"
        echo -e "Arguments:\n"
        echo -e " -h | --help                   Print this message"
        echo -e " -p | --proc                   Analyze current processes"
        echo -e " -u | --user	                Search for suspicious files in a specific user home"
	echo -e " -a | --affected		Find affected users by referrer $REFERRER"

}

function check_quarantine {
	[[ $(find /opt/cxs/quarantine/cxsuser/$1 -type f | wc -l) = "0" ]] && echo -e "${GREEN}Users quarantine empty.${NC}" && IFS=$IFSbak && exit
	du -sh /opt/cxs/quarantine/cxsuser/$1
	read -r -p "Do you wish to empty $1 quarantine? (y/n): " response
        case $response in
                [yY][eE][sS]|[yY] )
                        find /opt/cxs/quarantine/cxsuser/$1/ -type f -delete
                        echo -e "${GREEN}Deleted${NC}"
                        du -sh /opt/cxs/quarantine/cxsuser/$1/
                        df -h | head -2
                        ;;
                [nN][oO]|[nN] )
                        echo -e "Nothing is done, exiting...\n"
                        ;;
        esac
	IFS=$IFSbak
	exit
}

function rule {
        if [[ -z $(/sbin/iptables -L INPUT -n | grep "?zzz=Sy1LzNHIMTQp11DKKCkpsNLXz8ssTi1PLCnRK8kv0E") ]]; then
                echo -e "${RED}Recommended iptables rule not found!${NC}"
                echo "recommended rule is: DROP tcp multiport dports 80,443 STRING match "?zzz=Sy1LzNHIMTQp11DKKCkpsNLXz8ssTi1PLCnRK8kv0E" ALGO name kmp TO 1024"
                read -r -p "Do you wish to apply it? (y/n): " ruleanswer
                        [[ $ruleanswer = "y" ]] && hex=$(echo -n "?zzz=Sy1LzNHIMTQp11DKKCkpsNLXz8ssTi1PLCnRK8kv0E"|od -A n -t x1 -w100|sed 's/ //g') && iptables -I INPUT -p tcp -m tcp --match multiport --dports 80,443 -m string --algo kmp --to 1024 --hex-string "|$hex|" -j DROP -m comment --comment "PHP-Exploit P1315 (CXS)";
        else
                echo -e "${GREEN}Correct iptables rule present${NC}"
        fi

}
if [ "$1" = "-p" ] || [ "$1" = "--proc" ]; then

PSOUT=$(ps faux | grep '\.php')
IFSbak=$IFS; IFS=$'\n'

lSPROCS=""

for i in $PSOUT
    do [[ $(echo $i | awk -F'/' '{print $NF}' | grep '^\(.\)\1\{1,\}') ]] && LSPROCS+="$i\n"
done
[[ $LSPROCS = "" ]] && IFS=$IFSbak && echo -e "${GREEN}No suspicious processes${NC}" && exit

LSFILES=""

for i in $(echo -e $LSPROCS)
    do LSFILES+="$(pwdx $(echo $i | awk '{print $2}') | awk '{print $2}')/$(echo $i | awk -F'/' '{print $NF'})\n"
done

echo -e "${RED}Potential viruses:${NC}"
for i in $(echo -e $LSFILES); do [[ $(file -b $i) = "data" && $(du -k $i | cut -f1) = "4" ]] && echo $i; done
read -r -p "Do you wish to chmod 000 + chown root: these files? (y/n): " response
	case $response in
		[yY][eE][sS]|[yY] )
			for i in $(echo -e $LSFILES); do chmod 000 $i -v; chown root:root $i -v; done
			;;
		[nN][oO]|[nN] )
			echo -e "${GRAY}Nothing is done, exiting...${NC}\n"
			;;
	esac

IFS=$IFSbak
exit

elif [ "$1" = "-u" ] || [ "$1" = "--user" ]; then
	if [[ $2 = "" ]]; then echo "Error: No username given"; exit;
	fi
	if [[ $(id -u $2) -lt 500 ]]; then echo "Error: Invalid username"; exit;
	fi
	[[ -f /usr/bin/cl-quota ]] && USERINODES=$(/usr/bin/cl-quota -u $2 | tail -1 | awk '{print $2}')
	if [[ $USERINODES -gt 100000 ]]; then
		echo -e "${RED}The user has $USERINODES inodes!${NC}"; read -r -p "Are you sure you wish to preceed? (y/n): " proceed
	       if [[ $proceed != "y" ]]; then exit
	       fi
	fi

	IFSbak=$IFS; IFS=$'\n'
	LSVIRUS=""
	LSFILES=$(find /home/$2/ -type f ! -user root ! -perm 000 -name "*.php")
	for i in $LSFILES; do [[ $(echo $i | awk -F'/' '{print $NF}' | grep '^\(.\)\1\{1,\}') ]] && LSVIRUS+="$i\n"; done
	[[ $LSVIRUS = "" ]] && IFS=$IFSbak && echo -e "${GREEN}No suspicious files found${NC}" && check_quarantine $2
	POTVIRUS=""
	for i in $(echo -e $LSVIRUS); do [[ $(file -b $i) = "data" && $(du -k $i | cut -f1) = "4" ]] && POTVIRUS+="$i\n"; done
	[[ "$POTVIRUS" = "" ]] && IFS=$IFSbak && echo -e "${GREEN}No suspicious files${NC}" && check_quarantine $2
	echo -e "${RED}Potential viruses:${NC}"
	echo -e "$POTVIRUS"

read -r -p "Do you wish to chmod 000 + chown root: these files? (y/n): " response
       case $response in
               [yY][eE][sS]|[yY] )
                       for i in $(echo -e $POTVIRUS); do chmod 000 $i -v; chown root:root $i -v; done
                       ;;
               [nN][oO]|[nN] )
                       echo -e "${GRAY}Nothing is done...${NC}\n"
                       ;;
       esac
	check_quarantine $2
elif [ "$1" = "-a" ] || [ "$1" = "--affected" ]; then
	echo -e "${GREY}searching...${NC}"
	LSUSERS=$(for i in $(grep $REFERRER /usr/local/apache/logs/domlogs/* -l 2>/dev/null | cut -d/ -f7 | sed 's/-ssl_log//g'); do /scripts/whoowns $i; done | sort | uniq | sed 's/ /\n/g')
	if [[ -n $LSUSERS ]]; then
		echo -e "${RED}Affected users:${NC}\n$LSUSERS"
		echo -e "${RED}Abusive IPs:${NC}"
		grep $REFERRER /usr/local/apache/logs/domlogs/* 2>/dev/null | awk '{print $1}' | cut -d: -f2 | sort | uniq
		echo -e "${RED}Current quarantine size:${NC}"
		for i in $(echo -e "$LSUSERS"); do
			[[ $(find /opt/cxs/quarantine/cxsuser/$i/ -type f | wc -l) = "0" ]] && echo -e "${GREEN}$i quarantine empty"${NC} || du -sh /opt/cxs/quarantine/cxsuser/$i;
		done
		echo -e "${RED}Current free space:${NC}"
		df -h | head -2
		IFS=$IFSbak
		exit
	else
		echo -e "${GREEN}No affected users found!${NC}"
		IFS=$IFSbak
		exit
	fi
else
	usage
        read -r -p "Do you wish to check if recommended IPTables rule is present? (y/n): " ipresponse
        case $ipresponse in
                [yY][eE][sS]|[yY] )
                        rule
                        ;;
        esac
fi
